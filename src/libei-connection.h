
/* SPDX-License-Identifier: MIT */
/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include "util-object.h"
#include "util-list.h"
#include "brei-shared.h"

struct ei;
struct ei_connection;

/* This is a protocol-only object, not exposed in the API */
struct ei_connection {
	struct object object;
	struct brei_object proto_object;

	struct list pending_callbacks;
};

OBJECT_DECLARE_GETTER(ei_connection, context, struct ei*);
OBJECT_DECLARE_GETTER(ei_connection, version, uint32_t);
OBJECT_DECLARE_GETTER(ei_connection, id, object_id_t);
OBJECT_DECLARE_GETTER(ei_connection, proto_object, const struct brei_object*);
OBJECT_DECLARE_GETTER(ei_connection, interface, const struct ei_connection_interface *);
OBJECT_DECLARE_REF(ei_connection);
OBJECT_DECLARE_UNREF(ei_connection);

struct ei_connection *
ei_connection_new(struct ei *ei, object_id_t id, uint32_t version);

/**
 * Called when the ei_callback.done event is received after
 * an ei_connection_sync() request.
 */
typedef void (*ei_connection_sync_callback_t)(struct ei_connection *connection,
					      void *user_data);

void
ei_connection_sync(struct ei_connection *connection,
		  ei_connection_sync_callback_t callback,
		  void *user_data);
