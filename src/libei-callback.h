
/* SPDX-License-Identifier: MIT */
/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN callback WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include "util-object.h"
#include "util-list.h"
#include "brei-shared.h"

struct ei;
struct ei_callback;

typedef void (*ei_callback_func)(struct ei_callback *callback, void *callback_data, uint32_t proto_data);

/* This is a protocol-only object, not exposed in the API */
struct ei_callback {
	struct object object;
	struct brei_object proto_object;
	void *user_data; /* Note: user-data is attached to the object */

	struct list link; /* for use by the callers, if needed */

	ei_callback_func func;
	void *callback_data; /* Note: callback-data is attached to the callback */
};

OBJECT_DECLARE_GETTER(ei_callback, context, struct ei*);
OBJECT_DECLARE_GETTER(ei_callback, proto_object, const struct brei_object *);
OBJECT_DECLARE_GETTER(ei_callback, id, object_id_t);
OBJECT_DECLARE_GETTER(ei_callback, interface, const struct ei_callback_interface *);
OBJECT_DECLARE_GETTER(ei_callback, user_data, void *);
OBJECT_DECLARE_SETTER(ei_callback, user_data, void *);
OBJECT_DECLARE_REF(ei_callback);
OBJECT_DECLARE_UNREF(ei_callback);

struct ei_callback *
ei_callback_new(struct ei *ei, ei_callback_func func, void *callback_data);

struct ei_callback *
ei_callback_new_for_id(struct ei *ei, object_id_t id, uint32_t version);
