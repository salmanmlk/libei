---
title: EI Protocol documentation
draft: false
archetype: "home"
alwaysopen: true
---

**libei** is a library for Emulated Input, primarily aimed at the Wayland
stack. It uses a typical client/server separation, with the two parts connected
via a UNIX socket. In libei parlance, the client-side is called **"EI client"**, the
server side, typically a Wayland compositor, is called the **"EIS Implementation"**
(Emulated Input Server). These terms are used throughout this documentation.

This documentation details the protocol to communicate between the client side
and the EIS implementation.

A typical setup looks like this:

{{< mermaid >}}
graph LR;
    libwayland-server --> c1
    libwayland-server --> c2
    /dev/input/event0 ---> libinput
    libeis-- ei protocol ---libei
    subgraph Wayland Compositor
      libwayland-server
      libinput
      libeis
    end
    subgraph Wayland client A
      libei
      c1[libwayland-client]
    end
    subgraph Wayland client B
      c2[libwayland-client]
    end
{{< /mermaid >}}


The ei protocol is a public protocol that may be used directly by clients or
EIS implementations. This documentation describes the protocol, its interfaces
and how to generate language bindings.

{{% notice style="warning" %}}
The protocol is not yet declared stable.
{{% /notice %}}

If you are looking for easy-to-use C libraries instead, see:

- 🥚 [libei](https://libinput.pages.freedesktop.org/libei/api/group__libei.html) for the client side
- 🍦 [libeis](https://libinput.pages.freedesktop.org/libei/api/group__libeis.html) for the EIS implementation side
- 🚌 [liboeffis](https://libinput.pages.freedesktop.org/libei/api/group__liboeffis.html) is an helper library for DBus communication with the
  XDG RemoteDesktop portal (`liboeffis`)


# Documentation

{{% children  %}}
