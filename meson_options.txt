option('documentation', type: 'array', value: [], choices: ['api', 'protocol'], description: 'Enable documentation builds')
option('tests', type: 'boolean', value: 'true', description: 'Enable/disable tests')
option('liboeffis', type: 'feature', value: 'auto', description: 'Build liboeffis.so')
