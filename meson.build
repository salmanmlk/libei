project('libei', 'c',
  version: '0.4.1',
  license: 'MIT',
  default_options: [ 'c_std=gnu99', 'warning_level=2' ],
  meson_version: '>= 0.57.0')

pkgconfig = import('pkgconfig')
fs = import('fs')

cc = meson.get_compiler('c')
cflags =[
    '-Wno-unused-parameter',
    '-Wmissing-prototypes',
    '-Wno-missing-field-initializers',
    '-Wstrict-prototypes',
    '-Wstrict-prototypes',
    '-Wlogical-op',
    '-Wpointer-arith',
    '-Wuninitialized',
    '-Winit-self',
    '-Wstrict-prototypes',
    '-Wimplicit-fallthrough',
    '-Wredundant-decls',
    '-Wincompatible-pointer-types',
    '-Wformat=2',
    '-Wformat-overflow=2',
    '-Wformat-signedness',
    '-Wformat-truncation=2',
    '-Wmissing-declarations',
    '-Wshift-overflow=2',
    '-Wstrict-overflow=2',
    '-Wswitch',
]

if cc.get_id() == 'clang'
    cflags += [
        # clang doesn't think just using _unref_ is a use of the variable
        #       _unref_(foo) *bar = something_that_gives_a_ref
        # but we make heavy use of that in the test suite for convenience
        # of events we know must exist but we don't care about specifically
        '-Wno-unused-variable',
    ]
endif

add_project_arguments(cc.get_supported_arguments(cflags), language: 'c')

inc_builddir = include_directories('.')
inc_src = include_directories('src')

config_h = configuration_data()
config_h.set('_GNU_SOURCE', '1')
config_h.set_quoted('EI_VERSION', meson.project_version())
config_h.set_quoted('EIS_VERSION', meson.project_version())

dep_libxkbcommon = dependency('xkbcommon', required: false)
config_h.set10('HAVE_LIBXKBCOMMON', dep_libxkbcommon.found())
dep_libevdev = dependency('libevdev', required: false)
config_h.set10('HAVE_LIBEVDEV', dep_libevdev.found())
dep_systemd = dependency('libsystemd', required: get_option('liboeffis'))

configure_file(output: 'config.h', configuration: config_h)

subdir('proto')
subdir('src')
subdir('tools')
subdir('test')
subdir('doc')

black = find_program('black', required: false)
if black.found()
    test('python-black', black,
         args: ['--check', meson.project_source_root(), meson.project_source_root() / 'proto' / 'ei-scanner'],
         suite: 'python',
    )
endif

ruff = find_program('ruff', required: false)
if ruff.found()
    test('python-ruff', ruff,
         args: ['check', '--ignore=E741,E501', meson.project_source_root(), meson.project_source_root() / 'proto' / 'ei-scanner'],
         suite: 'python',
    )
endif

